module.exports = async function(req, res, proceed) {
  if (!req.session.userId) {
    return res.forbidden();
  }

  let user = await User.findOne({
    id: req.session.userId
  });

  if (user.isAdmin) {return proceed();}

  return res.forbidden();
};
