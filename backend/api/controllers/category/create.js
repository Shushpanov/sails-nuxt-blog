module.exports = {
  friendlyName: 'Create',

  description: 'Create category.',

  inputs: {
    title: {
      type: 'string',
      required: true,
      unique: true
    },

    description: {
      type: 'string'
    }
  },

  fn: async function(inputs, exits) {
    let newCategory = await Category.create(inputs).fetch();

    return exits.success(newCategory);
  }
};
