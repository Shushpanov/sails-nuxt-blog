module.exports = {
  friendlyName: 'Create',

  description: 'Create comment.',

  inputs: {
    content: {
      type: 'string',
      required: true
    },

    username: {
      type: 'string'
    },

    email: {
      type: 'string',
      isEmail: true
    },

    post: {
      type: 'number',
      required: true
    }
  },

  exits: {
    noUser: {
      statusCode: 401,
      description: 'There is no user attached to the comment'
    }
  },

  fn: async function(inputs, exits) {
    if (this.req.session.userId) {
      inputs = Object.assign(inputs, { user: this.req.session.userId });
    } else if (inputs.email && inputs.username) {
      inputs = Object.assign(
        inputs,
        { email: inputs.email },
        { username: inputs.username }
      );
    } else {
      throw 'noUser';
    }

    let newComment = await Comment.create(
      Object.assign(inputs, {
        post: inputs.post
      })
    ).fetch();

    return exits.success(newComment);
  }
};
