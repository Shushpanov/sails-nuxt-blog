module.exports = {
  friendlyName: 'Update',

  description: 'Update post.',

  inputs: {
    title: {
      type: 'string',
      required: true,
      unique: true
    },

    content: {
      type: 'string'
    },

    id: {
      type: 'number',
      required: true
    },

    categories: {
      type: 'json'
    }
  },

  fn: async function(inputs, exits) {
    let updatedPost = await Post.update({ id: inputs.id })
      .set({
        title: inputs.title,
        content: inputs.content
      })
      .fetch();

    if (inputs.categories) {
      await Post.replaceCollection(
        updatedPost.id,
        'categories',
        inputs.categories
      );
    }

    return updatedPost ? exits.success(updatedPost) : exits.notFound();
  }
};
