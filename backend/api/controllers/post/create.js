module.exports = {
  friendlyName: 'Create',

  description: 'Create post.',

  inputs: {
    title: {
      type: 'string',
      required: true,
      unique: true
    },

    content: {
      type: 'string'
    },

    categories: {
      type: 'json'
    }
  },

  fn: async function(inputs, exits) {
    let newPost = await Post.create(
      Object.assign(inputs, { user: this.req.session.userId })
    ).fetch();

    if (inputs.categories) {
      await Post.addToCollection(newPost.id, 'categories', inputs.categories);
    }

    return exits.success(newPost);
  }
};
