module.exports = {
  friendlyName: 'Delete',

  description: 'Delete user.',

  inputs: {
    password: {
      type: 'string'
    }
  },

  exits: {
    badCombo: {
      description: 'Password provided does not match the current user',
      statusCode: 401
    }
  },

  fn: async function(inputs, exits) {
    let user = await User.findOne({
      id: this.req.session.userId
    });

    await sails.helpers.passwords
      .checkPassword(inputs.password, user.password)
      .intercept('incorrect', 'badCombo');

    await User.destroy({ id: user.id });

    delete this.req.session.userId;

    return exits.success();
  }
};
