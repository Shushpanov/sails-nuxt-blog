module.exports = {
  friendlyName: 'Update',

  description: 'Update user.',

  inputs: {
    emailAddress: {
      type: 'string',
      required: true,
      isEmail: true,
      maxLength: 200,
      example: 'serge@magicmicro.com'
    },

    fullName: {
      type: 'string',
      required: true,
      maxLength: 50,
      example: 'Serge S.'
    },

    password: {
      type: 'string',
      required: true,
      example: '_[`~(xY([H9}vD`"'
    },
    confirmPassword: {
      type: 'string',
      example: '_[`~(xY([H9}vD`"'
    }
  },

  exits: {
    emailNotUnique: {
      statusCode: 409, // Set status as conflict
      description: 'The email address provided is already in use'
    },

    badCombo: {
      description: 'Password provided does not match the current user',
      statusCode: 401
    },

    nothingToUpdate: {
      statusCode: 400,
      description: 'Nothing to update'
    }
  },

  fn: async function(inputs) {
    let user = await User.findOne({
      id: this.req.session.userId
    });

    let updatedValues = {};

    let newEmailAddress =
      inputs.emailAddress !== undefined
        ? inputs.emailAddress.toLowerCase()
        : undefined;

    if (
      newEmailAddress !== undefined &&
      newEmailAddress !== user.emailAddress
    ) {
      if (
        await User.findOne({
          or: [
            { emailAddress: newEmailAddress },
            { emailRequested: newEmailAddress }
          ]
        })
      ) {
        throw 'emailNotUnique';
      }

      updatedValues = Object.assign(updatedValues, {
        emailRequested: inputs.emailAddress.toLowerCase(),
        emailStatus: 'newEmail',
        emailVerifyToken: await sails.helpers.strings.random('url-friendly'),

        emailVerifyTokenExpiry: Date.now() + 24 * 60 * 60 * 1000
      });
    }

    if (inputs.fullName !== undefined && inputs.fullName !== user.fullName) {
      updatedValues = Object.assign(updatedValues, {
        fullName: inputs.fullName
      });
    }

    if (inputs.password) {
      await sails.helpers.passwords
        .checkPassword(inputs.confirmPassword, user.password)
        .intercept('incorrect', 'badCombo');
      updatedValues = Object.assign(updatedValues, {
        password: await sails.helpers.passwords.hashPassword(inputs.password)
      });
    }

    if (!Object.keys(updatedValues).length) {
      throw 'nothingToUpdate';
    }

    let updatedUser = await User.update({ id: user.id })
      .set(updatedValues)
      .fetch();

    return updatedUser ? exits.success(updatedUser) : exits.notFound;
  }
};
