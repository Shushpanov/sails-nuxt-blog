module.exports = {
  friendlyName: 'Login',

  description: 'Login user.',

  inputs: {
    emailAddress: {
      type: 'string',
      required: true
    },

    password: {
      type: 'string',
      required: true
    },

    rememberMe: {
      description: 'Whether the user wishes to extend the session length.',
      type: 'boolean'
    }
  },

  exits: {
    badCombo: {
      description: 'Password provided does not match the current user',
      statusCode: 401
    }
  },

  fn: async function(inputs, exits) {
    let user = await User.findOne({
      emailAddress: inputs.emailAddress.toLowerCase()
    });

    if (!user) {throw "badCombo";}

    await sails.helpers.passwords
      .checkPassword(inputs.password, user.password)
      .intercept('incorrect', 'badCombo');

    if (inputs.rememberMe)
      {this.req.session.cookie.maxAge = 30 * 24 * 60 * 60 * 1000;}

    this.req.session.userId = user.id;

    return exits.success(user);
  }
};
