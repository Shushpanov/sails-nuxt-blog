module.exports = {
  friendlyName: 'Create',

  description: 'Create user.',

  inputs: {
    emailAddress: {
      type: 'string',
      required: true,
      isEmail: true,
      maxLength: 200,
      example: 'serge@magicmicro.com'
    },

    fullName: {
      type: 'string',
      required: true,
      maxLength: 50,
      example: 'Serge S.'
    },

    password: {
      type: 'string',
      required: true,
      example: '_[`~(xY([H9}vD`"'
    }
  },

  exits: {
    emailNotUnique: {
      responseType: 'emailNotUnique',
      description: 'The email address provided is already in use'
    }
  },

  fn: async function(inputs) {
    let newUser = await User.create({
      emailAddress: inputs.emailAddress.toLowerCase(),
      fullName: inputs.fullName,
      password: await sails.helpers.passwords.hashPassword(inputs.password),
      emailVerifyToken: await sails.helpers.strings.random('url-friendly'),
      emailVerifyTokenExpiry: Date.now() + 24 * 60 * 60 * 1000,
      isAdmin: true
    })
      // Intercept the E_UNIQUE response with the emailNotUnique response
      .intercept('E_UNIQUE', 'emailNotUnique')
      // Fetch user if created
      .fetch();

    this.req.session.userId = newUser.id;

    // Send email verify email
    await sails.helpers.sendEmail.with({
      to: newUser.emailAddress,
      subject: 'Verify your account!',
      template: 'verify-account',
      templateData: {
        fullName: newUser.fullName,
        token: newUser.emailVerifyToken
      }
    });

    return exits.success(newUser);
  }
};
