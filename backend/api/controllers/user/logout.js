module.exports = {
  friendlyName: 'Logout',

  description: 'Logout user.',

  fn: async function(inputs, exits) {
    delete this.req.session.userId;

    return exits.success();
  }
};
