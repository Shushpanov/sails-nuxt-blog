/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
  // User API endpoints
  'POST  /api/v1/user/create': { action: 'user/create' },
  'PUT  /api/v1/user/update': { action: 'user/update' },
  'DELETE  /api/v1/user/delete': { action: 'user/delete' },

  'PUT  /api/v1/user/login': { action: 'user/login' },
  'GET  /api/v1/user/logout': { action: 'user/logout' },
  'GET /api/v1/user/check': { action: 'user/check' },

  // Post API endpoints
  'POST  /api/v1/post/create': { action: 'post/create' },
  'PUT  /api/v1/post/update': { action: 'post/update' },
  'DELETE  /api/v1/post/delete': { action: 'post/delete' },

  // Comment API endpoints
  'POST  /api/v1/comment/create': { action: 'comment/create' },
  'DELETE  /api/v1/comment/delete': { action: 'comment/delete' },

  // Category API endpoints
  'POST  /api/v1/category/create': { action: 'category/create' },
  'PUT  /api/v1/category/update': { action: 'category/update' },
  'DELETE  /api/v1/category/delete': { action: 'category/delete' }
};
